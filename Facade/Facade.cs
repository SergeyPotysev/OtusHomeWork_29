﻿using Facade.Subsystem;

namespace Facade
{
    // Класс Фасада предоставляет простой интерфейс для сложной логики одной или
    // нескольких подсистем. Фасад делегирует запросы клиентов соответствующим
    // объектам внутри подсистемы. Фасад также отвечает за управление их
    // жизненным циклом. Все это защищает клиента от нежелательной сложности
    // подсистемы.
    public class Facade
    {
        protected Subsystem1 _subsystem1;
        protected Subsystem2 _subsystem2;

        public Facade(Subsystem1 subsystem1, Subsystem2 subsystem2)
        {
            _subsystem1 = subsystem1;
            _subsystem2 = subsystem2;
        }


        // Методы Фасада удобны для быстрого доступа к сложной функциональности
        // подсистем. Однако клиенты получают только часть возможностей
        // подсистемы.
        public void Operation()
        {
            // Фасад инициализирует подсистемы
            _subsystem1.Operation_1();
            _subsystem2.Operation_1();

            // Фасад запускает выполнение подсистемами необходимых действий
            _subsystem1.Operation_N();
            _subsystem2.Operation_Z();
        }
    }
}

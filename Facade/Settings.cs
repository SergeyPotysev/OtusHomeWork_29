﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;

namespace Facade
{
    class Settings
    {
        /// <summary>
        /// Объект, содержащий начальные установки.
        /// </summary>
        private static Settings _settings = null;

        /// <summary>
        /// Ширина элемента PictureBox.
        /// </summary>
        public int FrameWidth { get; private set; }

        /// <summary>
        /// Высота элемента PictureBox.
        /// </summary>
        public int FrameHeight { get; private set; }
      
        /// <summary>
        /// Номер, добавляемый к имени файла.
        /// </summary>
        public int FileNum { get; private set; }

        /// <summary>
        /// Список файлов с картинками.
        /// </summary>
        public List<string> ImageFile = new List<string>();

        /// <summary>
        /// Список подписей к файлам с картинками.
        /// </summary>
        public List<string> ImageText = new List<string>();

        /// <summary>
        /// Расстояние от края элемента PictureBox до рамки.
        /// </summary>
        public int Delta { get; private set; }

        private Settings()
        {
            IConfiguration AppConfiguration = new ConfigurationBuilder()
                 .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                 .Build();
            ImageFile.Add(Convert.ToString(AppConfiguration.GetSection("Image_1").Value));
            ImageFile.Add(Convert.ToString(AppConfiguration.GetSection("Image_2").Value));
            ImageText.Add(Convert.ToString(AppConfiguration.GetSection("Text_1").Value));
            ImageText.Add(Convert.ToString(AppConfiguration.GetSection("Text_2").Value));
            Delta = Convert.ToInt32(AppConfiguration.GetSection("Delta").Value);
        }


        public static Settings GetInstance()
        {
            if (_settings == null)
                _settings = new Settings();
            return _settings;
        }


        public Settings SetWidth(int width)
        {
            FrameWidth = width;
            return this;
        }


        public Settings SetHeight(int height)
        {
            FrameHeight = height;
            return this;
        }


        public void SetFileNum(int index)
        {
            FileNum = index;
        }

    }
}

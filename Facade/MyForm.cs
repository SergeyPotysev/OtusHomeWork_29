﻿using Facade.Subsystem;
using SkiaSharp;
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Windows.Forms;

namespace Facade
{
    public partial class MyForm : Form
    {
        public PictureBox Motivator
        {
            get { return MyPictureBox; }
            set { MyPictureBox = value; }
        }


        public MyForm()
        {
            InitializeComponent();
        }


        private void StartButton_Click(object sender, EventArgs e)
        {
            // В клиентском коде могут быть уже созданы некоторые объекты
            // подсистемы. В этом случае может оказаться целесообразным
            // инициализировать Фасад с этими объектами вместо того, чтобы
            // позволить Фасаду создавать новые экземпляры.
            Subsystem1 subsystem1 = new Subsystem1(MyPictureBox.Width, this);
            Subsystem2 subsystem2 = new Subsystem2(MyPictureBox.Height, this);
            Facade facade = new Facade(subsystem1, subsystem2);
            Client.ClientCode(facade);
        }


        private void ExitButton_Click(object sender, EventArgs e)
        {
            Dispose();
            Close();
        }


        private void MyForm_load(object sender, EventArgs e)
        {
            StartButton.Enabled = false;
            TurnOffCheckBox();
        }


        private void TurnOffCheckBox()
        {            
            CheckBox1.Checked = false;
            CheckBox2.Checked = false;
        }


        private void CheckBox1_CheckStateChanged(object sender, EventArgs e)
        {
            StartButton.Enabled = !StartButton.Enabled;
            if(CheckBox1.Checked)
            {
                Settings.GetInstance().SetFileNum(0); 
            }
            CheckBox2.Checked = false;
        }


        private void CheckBox2_CheckStateChanged(object sender, EventArgs e)
        {
            StartButton.Enabled = !StartButton.Enabled;
            if (CheckBox2.Checked)
            {
                Settings.GetInstance().SetFileNum(1);
            }
            CheckBox1.Checked = false;
        }


        private void MyPictureBox_Paint(object sender, PaintEventArgs e)
        {
            TurnOffCheckBox();
        }
    }
}

﻿using SkiaSharp;
using System.Drawing;
using System.IO;

namespace Facade.Subsystem
{
    // Подсистема может принимать запросы либо от фасада, либо от клиента
    // напрямую. В любом случае, для Подсистемы Фасад – это еще один клиент, и
    // он не является частью Подсистемы.
    public class Subsystem1
    {
        private readonly int _frameWidth;
        private readonly MyForm _board;

        public Subsystem1(int width, MyForm board)
        {
            _frameWidth = width;
            _board = board;
        }


        public void Operation_1()
        {
            // Get ready!
            Settings.GetInstance().SetWidth(_frameWidth);
        }


        public void Operation_N()
        {
            // Go!
            int width = Settings.GetInstance().FrameWidth;
            int height = Settings.GetInstance().FrameHeight;
            int delta = Settings.GetInstance().Delta;
            int fileNum = Settings.GetInstance().FileNum;
            string imageText = Settings.GetInstance().ImageText[fileNum];
            string imageFile = Settings.GetInstance().ImageFile[fileNum];

            SKImageInfo imageInfo = new SKImageInfo(width, height);
            using SKSurface surface = SKSurface.Create(imageInfo);
            SKCanvas canvas = surface.Canvas;

            // Создать фон указанного цвета, но сначала стереть все остальное на холсте
            canvas.Clear(SKColors.White);

            // Прочитать картинку из файла 
            Stream fileStream = File.OpenRead(imageFile);

            // Декодировать растровое изображение из потока
            using (var stream = new SKManagedStream(fileStream))
            using (var bitmap = SKBitmap.Decode(stream))
            using (var paint = new SKPaint())
            {
                canvas.DrawBitmap(bitmap, SKRect.Create(0, 0, width, height), paint);
            }

            using (var paint = new SKPaint())
            {
                paint.IsAntialias = true;
                paint.Color = SKColors.White;
                paint.IsStroke = false;

                // Настроить свойство TextSize так, чтобы текст занимал 95% ширины элемента PictureBox
                float textWidth = paint.MeasureText(imageText);
                paint.TextSize = 0.95f * width * paint.TextSize / textWidth;

                // Найти границы текста
                SKRect textBounds = new SKRect();
                paint.MeasureText(imageText, ref textBounds);
                float xText = width / 2 - textBounds.MidX;

                // Добавить текст
                canvas.DrawText(imageText, xText, height - 2 * delta, paint);
                paint.IsStroke = true;

                // Добавить рамку
                canvas.DrawRect(delta, delta, width - 2 * delta, height - 2 * delta, paint);
            }

            // Добавить картинку в элемент PictureBox
            using SKImage image = surface.Snapshot();
            using SKData data = image.Encode(SKEncodedImageFormat.Png, 100);
            using MemoryStream mStream = new MemoryStream(data.ToArray());
            Bitmap bm = new Bitmap(mStream, false);
            _board.Motivator.Image = bm;
        }
    }
}
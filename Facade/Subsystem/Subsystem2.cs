﻿using System.Drawing.Imaging;

namespace Facade.Subsystem
{
    // Некоторые фасады могут работать с разными подсистемами одновременно.
    public class Subsystem2
    {
        private readonly int _height;
        private readonly MyForm _board;

        public Subsystem2(int height, MyForm board)
        {
            _height = height;
            _board = board;
        }


        public void Operation_1()
        {
            // Get ready!
            Settings.GetInstance().SetHeight(_height);
        }


        public void Operation_Z()
        {
            // Fire!
            _board.Motivator.Image.Save($"image_{Settings.GetInstance().FileNum + 1}.png", ImageFormat.Png);            
        }
    }
}

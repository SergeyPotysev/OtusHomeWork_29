# ДЗ OTUS по теме «Структурные шаблоны проектирования»

Использована структура классов из книги ["Погружение в ПАТТЕРНЫ ПРОЕКТИРОВАНИЯ"](https://refactoring.guru/ru/design-patterns/book), Александр Швец.

Приложение использует следующие паттерны:  
+ Singleton;
+ Builder;
+ Facade.

## Файлы настроек

`appsettings.json` - исходные данные для создания мотиваторов.  

## Краткое описание созданных классов

`Settings` - класс для хранения и доступа к исходным данным и настройкам.  
`MyForm` - Windows Forms класс с элеменами управления и обработкой событий.  
`Subsystem1` - подсистема реализации задачи №1.  
`Subsystem2` - подсистема реализации задачи №2.  
`Facade` - класс Фасада предоставляет простой интерфейс для сложной логики одной или нескольких подсистем.  
`Client` - клиентский код работы со сложными подсистемами через простой интерфейс, предоставляемый Фасадом.  